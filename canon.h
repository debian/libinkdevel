/* canon.h
 *
 * (c) 2009, 2014 Thierry Merle, Louis Lagendijk, Markus Heinz
 *
 * This software is licensed under the terms of the GPL.
 * For details see file COPYING.
 */

#ifndef CANON_H
#define CANON_H

int get_ink_level_canon(const int port, const char* device_file,
			const int portnumber, struct ink_level *level);
#endif
