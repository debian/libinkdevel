/* canon_xml.c
 *
 * (c) 2022 Markus Heinz
 *
 * This software is licensed under the terms of the GPL.
 * For details see file COPYING.
 */

#include <stdio.h>
#include <stdlib.h>

#include <libxml/tree.h>
#include <libxml/parser.h>
#include <libxml/xpath.h>
#include <libxml/xpathInternals.h>

#include "config.h"
#include "inklevel.h"
#include "canon_xml.h"

static int execute_xpath_expression(const xmlChar* xml,
				    const xmlChar* xpathExpr,
				    const xmlChar* nsList,
				    struct ink_level *level);

static int register_namespaces(xmlXPathContextPtr xpathCtx,
			       const xmlChar* nsList);

static void evaluate_xpath_nodes(xmlNodeSetPtr nodes, struct ink_level *level);

int parse_level_from_canon_xml(const unsigned char *xml,
			       struct ink_level *level) {
  int result = OK;

  xmlInitParser();
  xmlChar *xpath = BAD_CAST XPATH_EXPRESSION;
  xmlChar *ns = BAD_CAST XPATH_NS;

  result = execute_xpath_expression((BAD_CAST xml), xpath, ns, level);

  if (result != OK) {
    result = COULD_NOT_PARSE_RESPONSE_FROM_PRINTER;
  }

  xmlCleanupParser();

  return result;
}

static int execute_xpath_expression(const xmlChar* xml,
				    const xmlChar* xpathExpr,
				    const xmlChar* nsList,
				    struct ink_level *level) {
  xmlDocPtr doc;
  xmlXPathContextPtr xpathCtx;
  xmlXPathObjectPtr xpathObj;

  /* Load XML document */
  doc = xmlReadDoc(xml, NULL, NULL, 0);

  if (doc == NULL) {

#ifdef DEBUG
    printf("Error: unable to parse XML\n");
#endif

    return ERROR;
  }

  /* Create xpath evaluation context */
  xpathCtx = xmlXPathNewContext(doc);

  if(xpathCtx == NULL) {

#ifdef DEBUG
    printf("Error: unable to create new XPath context\n");
#endif

    xmlFreeDoc(doc);
    return ERROR;
  }

  /* Register namespaces from list (if any) */
  if((nsList != NULL) && (register_namespaces(xpathCtx, nsList) < 0)) {

#ifdef DEBUG
    printf("Error: failed to register namespaces list \"%s\"\n", nsList);
#endif

    xmlXPathFreeContext(xpathCtx);
    xmlFreeDoc(doc);
    return ERROR;
  }

  /* Evaluate xpath expression */
  xpathObj = xmlXPathEvalExpression(xpathExpr, xpathCtx);

  if(xpathObj == NULL) {

#ifdef DEBUG
    printf("Error: unable to evaluate xpath expression \"%s\"\n", xpathExpr);
#endif

    xmlXPathFreeContext(xpathCtx);
    xmlFreeDoc(doc);
    return ERROR;
  }

  /* Print results */
  evaluate_xpath_nodes(xpathObj->nodesetval, level);

  /* Cleanup */
  xmlXPathFreeObject(xpathObj);
  xmlXPathFreeContext(xpathCtx);
  xmlFreeDoc(doc);

  return OK;
}

static int register_namespaces(xmlXPathContextPtr xpathCtx,
			       const xmlChar* nsList) {
  xmlChar* nsListDup;
  xmlChar* prefix;
  xmlChar* href;
  xmlChar* next;

  nsListDup = xmlStrdup(nsList);

  if(nsListDup == NULL) {

#ifdef DEBUG
    printf("Error: unable to strdup namespaces list\n");
#endif

    return ERROR;
  }

  next = nsListDup;

  while(next != NULL) {
    /* skip spaces */
    while((*next) == ' ') {
      next++;
    }

    if((*next) == '\0') {
      break;
    }

    /* find prefix */
    prefix = next;
    next = (xmlChar*)xmlStrchr(next, '=');

    if(next == NULL) {

#ifdef DEBUG
      printf("Error: invalid namespaces list format\n");
#endif

      xmlFree(nsListDup);
      return ERROR;
    }

    *(next++) = '\0';

    /* find href */
    href = next;
    next = (xmlChar*)xmlStrchr(next, ' ');

    if(next != NULL) {
      *(next++) = '\0';
    }

    /* do register namespace */
    if(xmlXPathRegisterNs(xpathCtx, prefix, href) != 0) {

#ifdef DEBUG
      printf("Error: unable to register NS with "
	      "prefix=\"%s\" and href=\"%s\"\n", prefix, href);
#endif

      xmlFree(nsListDup);
      return ERROR;
    } else {

#ifdef DEBUG
      printf("namespaces registered\n");
#endif

    }
  }

  xmlFree(nsListDup);
  return OK;
}

static void evaluate_xpath_nodes(xmlNodeSetPtr nodes, struct ink_level *level) {
  xmlNodePtr cur;
  int size;
  int i;
  int colorsAdded = 0;

  size = (nodes) ? nodes->nodeNr : 0;

#ifdef DEBUG
  printf("Result (%d nodes):\n", size);
#endif

  for(i = 0; i < size; ++i) {
    if(nodes->nodeTab[i]->type == XML_NAMESPACE_DECL) {
      xmlNsPtr ns;

      ns = (xmlNsPtr)nodes->nodeTab[i];
      cur = (xmlNodePtr)ns->next;

#ifdef DEBUG
      if(cur->ns) {
	printf("= namespace \"%s\"=\"%s\" for node %s:%s\n",
	       ns->prefix, ns->href, cur->ns->href, cur->name);
      } else {
	printf("= namespace \"%s\"=\"%s\" for node %s\n",
	       ns->prefix, ns->href, cur->name);
      }
#endif

    } else if(nodes->nodeTab[i]->type == XML_ELEMENT_NODE) {
      cur = nodes->nodeTab[i];

#ifdef DEBUG
      if(cur->ns) {
	printf("= element node \"%s:%s\"\n", cur->ns->href, cur->name);
      } else {
	printf("= element node \"%s\"\n", cur->name);
      }
#endif

      xmlNode *children = nodes->nodeTab[i]->children;
      xmlChar *color = NULL;
      xmlChar *curlevel = NULL;

      while (children != NULL) {
	if (!xmlStrcmp(BAD_CAST "color", children->name)) {
	  color = xmlStrndup(xmlNodeGetContent(children), 80);
	} else if (!xmlStrcmp(BAD_CAST "level", children->name)) {
	  curlevel = xmlStrndup(xmlNodeGetContent(children), 10);
	}

	children = children->next;
      }

#ifdef DEBUG
      printf("color %s level %s\n", color, curlevel);
#endif

      // fill in struct ink_level
      level->status = RESPONSE_VALID;

      if (!xmlStrcasecmp(color, BAD_CAST "Color")) {
	level->levels[colorsAdded][INDEX_TYPE] = CARTRIDGE_COLOR;
	level->levels[colorsAdded][INDEX_LEVEL] = atoi((char*) curlevel);
	colorsAdded++;
      } else if (!xmlStrcasecmp(color, BAD_CAST "Black")) {
	level->levels[colorsAdded][INDEX_TYPE] = CARTRIDGE_BLACK;
	level->levels[colorsAdded][INDEX_LEVEL] = atoi((char *)curlevel);
	colorsAdded++;
      }

      free(color);
      free(curlevel);
    } else {
      cur = nodes->nodeTab[i];

#ifdef DEBUG
      printf("= node \"%s\": type %d\n", cur->name, cur->type);
#endif

    }
  }
}
