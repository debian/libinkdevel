/* libusb-utils.c
 *
 * (c) 2014, 2015, 2018, 2022 Markus Heinz
 *
 * This software is licensed under the terms of the GPL.
 * For details see file COPYING.
 */

#ifndef LIBUSB_UTILS_H
#define LIBUSB_UTILS_H

#include "config.h"
#include <libusb.h>

/* Return types */
#define USB_SUCCESS 1
#define USB_FAILURE 0

/* Timeout for data transfers in milliseconds */
#define USB_TIMEOUT 3000 

typedef struct usb_printer {
  libusb_device *device;
  libusb_device_handle *handle;
  uint8_t read_endp;
  uint8_t write_endp;
  uint8_t interface;
  uint8_t altsetting;
  uint8_t configuration;
} usb_printer;

int init_usb();
void shutdown_usb();

usb_printer *find_printer(int instance);
usb_printer *check_for_printer(libusb_device_handle *handle);

int open_device_handle(usb_printer *printer);
int release_device_handle(usb_printer *printer);

int get_usb_device_id(usb_printer *printer, char *buffer, size_t bufsize);

int bulk_transfer(libusb_device_handle *handle, uint8_t endp, char *buffer, 
		  size_t bufsize, int *transfered);

#endif
