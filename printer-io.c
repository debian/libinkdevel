/* printer-io.c
 *
 * (c) 2014, 2015, 2018, 2022 Markus Heinz
 *
 * This software is licensed under the terms of the GPL.
 * For details see file COPYING.
 */

#include "config.h"

#include <libusb.h>
#include "libusb-utils.h"

#include <stdio.h>

#include "printer-io.h"
#include "devices.h"
#include "inklevel.h"
#include "util.h"

#include <unistd.h>

/*
 * Open the needed connection to the printer. The connection details are stored
 * in the passed struct. If the connection can be established the connection is
 * stored in the passed struct for future reference.
 *
 * Returns PRINTER_SUCCESS or PRINTER_FAILURE.
 */
int open_wrapper(printer_descriptor *desc) {

  desc->mode = PRINTER_MODE_DEVICE;

  if (desc->port == USB) {
    desc->mode = PRINTER_MODE_LIBUSB;
  }

  if (desc->mode == PRINTER_MODE_LIBUSB) {
    if (init_usb() == USB_SUCCESS) {
      usb_printer *printer = find_printer(desc->portnumber);

      if (printer != NULL) {
	desc->printer = printer;

	if (open_device_handle(printer) == USB_SUCCESS) {
	  return PRINTER_SUCCESS;
	} else {
	  return PRINTER_FAILURE;
	}
      } else {
	return PRINTER_FAILURE;
      }
    }
  }
  
  return PRINTER_FAILURE;
}

/*
 * Closes the connection to the printer specified by the passed struct.
 *
 * Returns PRINTER_SUCCESS or PRINTER_FAILURE.
 */
int close_wrapper(printer_descriptor *desc) {

  if (desc->mode == PRINTER_MODE_LIBUSB) {
    if (desc->printer != NULL) {
      
      int result = release_device_handle(desc->printer);

      shutdown_usb();

      if (result == USB_SUCCESS) {
	return PRINTER_SUCCESS;
      } else {
	return PRINTER_FAILURE;
      }

    } else {

#ifdef DEBUG
      printf("printer handle is NULL\n");
#endif
      
      shutdown_usb();
      return PRINTER_FAILURE;
    }
  }

  return PRINTER_FAILURE;
}

/*
 * Tries to read bufsize characters from the printer specified by desc into
 * buffer. The actual number of read characters will be stored in transfered.
 *
 * The nonblocking parameter only comes into effect when device node based 
 * communication is performed. Libusb based reads are always nonblocking.
 *
 * Returns PRINTER_SUCCESS or PRINTER_FAILURE.
 */ 
int read_wrapper(printer_descriptor *desc, char *buffer, 
		 size_t bufsize, int *transfered, int nonblocking) {

  if (desc->mode == PRINTER_MODE_LIBUSB && desc->printer != NULL) {
    int result = bulk_transfer(desc->printer->handle, 
                               desc->printer->read_endp, buffer, bufsize, 
                               transfered);

    if (result == USB_SUCCESS) {
      return PRINTER_SUCCESS;
    }
  }

  return PRINTER_FAILURE;
}

/* 
 * Tries to write bufsize characters to the printer specified by desc stored in
 * buffer.  The actual number of written characters will be stored in 
 * transfered.
 * 
 * Returns PRINTER_SUCCESS or PRINTER_FAILURE.
 */
int write_wrapper(printer_descriptor *desc, char *buffer, 
		  size_t bufsize, int *transfered) {
  if (desc->mode == PRINTER_MODE_LIBUSB && desc->printer != NULL) {
    int result = bulk_transfer(desc->printer->handle, 
                               desc->printer->write_endp, buffer, bufsize, 
                               transfered);

    if (result == USB_SUCCESS) {
      return PRINTER_SUCCESS;
    }
  }
  
  return PRINTER_FAILURE;
}
