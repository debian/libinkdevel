/* canon_xml.h
 *
 * (c) 2022 Markus Heinz
 *
 * This software is licensed under the terms of the GPL.
 * For details see file COPYING.
 */

#ifndef CANON_XML_H
#define CANON_XML_H

#include <libxml/tree.h>
#include <libxml/parser.h>
#include <libxml/xpath.h>
#include <libxml/xpathInternals.h>

#define XPATH_EXPRESSION "//ivec:ink"
#define XPATH_NS "ivec=http://www.canon.com/ns/cmd/2008/07/common/"

int parse_level_from_canon_xml(const unsigned char *xml,
			       struct ink_level *level);
#endif
